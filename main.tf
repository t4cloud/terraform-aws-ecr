resource "aws_ecr_repository" "default" {
  name = "${var.name}"
}

resource "aws_ecr_repository_policy" "default" {
  repository = "${aws_ecr_repository.default.id}"
  policy     = "${data.aws_iam_policy_document.default.json}"
}

data "aws_iam_policy_document" "default" {
  "statement" {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = "${var.role_arn}"
    }

    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
    ]
  }
}

resource "aws_ecr_lifecycle_policy" "default" {
  repository = "${aws_ecr_repository.default.id}"

  policy = <<EOF
  {
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last N images",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": ${var.max_images_retained}
            },
            "action": {
                "type": "expire"
            }
        }
    ]
  }
EOF
}
